(function ($) {
  var document = window.document;
  Helpden = {
    init: function(options) {
      Helpden.createTab();
      Helpden.update(options);
    },

    update: function(settings) {
      var self = Helpden;
      self._configure(settings);
      if (!self._settings.url) {
        console && console.warn && console.warn("Helpden Dropbox must be configured with a URL.");
      } else {
        self._updateTab();
      }
    },

    _settings: {
      dropboxID:      null, 
      tabID:          "support",
      tabText:        "Live Support", 
      tabColor:       "#000000",
      tabPosition:    'Left'
    },

    _configure: function(options) {
      var prop;
      var self = this;
      for (prop in options) {
        if (options.hasOwnProperty(prop)) {
          self._settings[prop] = options[prop];
        }
      }
      self._prependSchemeIfNecessary('url');
    },

    _prependSchemeIfNecessary: function(urlProperty) {
      var url = this._settings[urlProperty];
      if (url && !(this._checkUrl.test(url))) {
        this._settings[urlProperty] = document.location.protocol + "//" + url;
      }
    },

    _checkUrl: /^[a-zA-Z]+:\/\//,

    _updateTab: function() {
        var tab = $("#Helpden_tab");
        tab.attr('title', this._settings.tabText);
        tab.attr('class', 'HelpdenTab' + this._settings.tabPosition.toLowerCase());
        tab.attr('className', 'HelpdenTab' + this._settings.tabPosition.toLowerCase()); 
        var post_url = this._settings.url + "?id="+this._settings.dropboxID+"&position="+this._settings.tabPosition.toLowerCase();
		tab.html('<iframe src="'+post_url+'" frameborder="0" scrolling="auto" allowTransparency="true" style="border:0;"></iframe>');
        tab.attr('style','display:block');
    },

    createTab: function() {
	  var tab = $('<a href="#" id="Helpden_tab" style="display:none;"></a>')[0];
      document.body.appendChild(tab);
    }
  
  };

  if (window.Helpden_params) {
    Helpden.init(window.Helpden_params);
  }

})(jQuery);
