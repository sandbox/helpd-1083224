<?php
// $Id: helpden.admin.inc,v 1.0.0.0 2011/03/02 06:34:14

/**
 * @file
 * Administration settings for Helpden Bulb integration
 */

/**
 * Administration settings form.
 *
 * @return
 *   The completed form definition.
 */
function helpden_admin_settings() {
  $form = array();

  global $base_path;

  $form['helpden_general_settings'] = array(
    '#type'  => 'fieldset',
    '#title' => t('General'),
  );
  $form['helpden_general_settings']['helpden_id'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Helpden ID'),
    '#description'   => t('Your Helpden ID.'),
    '#default_value' => variable_get('helpden_id', ''),
  );
   $form['helpden_general_settings']['helpden_postion'] = array(
    '#type'          => 'select',
    '#title'         => t('Position'),
    '#description'   => t('Position of Bulb.'),
    '#default_value' => variable_get('helpden_postion', 'left'),
    '#options'       => array(
      'topleft'  => t('Topleft'),
      'topright' => t('Topright'),
	  'right'    => t('Right'),
	  'left'     => t('Left'),
	  'bottomright' => t('Bottomright'),
	  'bottomleft'  => t('Bottomleft'),
    ),
  );
  $form['helpden_general_settings']['show_bulb'] = array(
    '#type'          => 'radios',
    '#title'         => t('Show bulb on your site'),
    '#default_value' => variable_get('show_bulb', '1'),
    '#description'   => t('Display Bulb icon your site.'),
	 '#options'       => array(
      1 => t('Yes'),
      0 => t('No')
    ),
  );
  return system_settings_form($form);
}
